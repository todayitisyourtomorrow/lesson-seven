import {useParams} from "react-router";

function UseParam() {
  const {id} = useParams()
  return(
    <>
      <div>
        salam {id}
      </div>
    </>
  )
}
export default UseParam