import data from './products'
import {Button, Card, Col, Container, Row} from "react-bootstrap";
function SecondPage(props) {

    const products = data.filter(item => item.discount && item.title.toLowerCase().startsWith(props.but.toLowerCase()) );
  // const products = props.name === 'discounted' ? data.filter(prev => prev.discount && prev.title.toLowerCase().startsWith(props.but.toLowerCase())
  // ) : props.name === 'home' ? data.filter(item => item.title.toLowerCase().startsWith(props.but.toLowerCase())
  // ) : data.filter(item => item.category_id === props.name && item.title.toLowerCase().startsWith(props.but.toLowerCase()))

  return(
    <>

      <h1>Товары со скидками  </h1>
      <div>

        <Container>
          <Row>
            {products.map((item) => {
              return(
                <Col className={'my-3'}>
                  <Card className={'Card'} >
                    {item.discount? <div className={'div'}>{item.discount} % </div>
                      :null}
                    <div style={{height:'250px', width:'280px'}}>
                      <div className={'divImg'} style={{backgroundImage: `url(${item.main_image.path.original})`}} />
                    </div>
                    <Card.Body style={{display:'flex', flexDirection:'column', justifyContent:'center', alignItems:'center'}}>
                      <Card.Title>{item.title}</Card.Title>

                      <Button variant="primary" className={'CardButton'}>
                        {props.curen === 'dollar' ? (item.price/ 87.2).toFixed(1) : props.curen === 'euro'? (item.price / 92).toFixed(1) :
                          <span>{item.price * (1- item.discount / 100 )} сом</span>}
                        {props.curen==='dollar'? ` $`:props.curen==='euro'? ` E`: null}
                        {item.discount ?
                          <>
                            <span style={{textDecoration: "line-through", color:'red', fontSize:'12px'}}> {item.price} c</span>
                          </>
                          :null}
                      </Button>
                      <div>
                        <span onClick={() => {
                          props.setCuren('dollar')
                        }} className={'cont'}>$</span>
                        <span onClick={() => {
                          props.setCuren('euro')
                        }} className={'cont'}>E</span>
                        <span onClick={() => {
                          props.setCuren('som')
                        }} className={'cont'}>C</span>
                      </div>
                    </Card.Body>
                  </Card>
                </Col>
              )
            })}
          </Row>
        </Container>
      </div>
    </>
  );
}
export default SecondPage