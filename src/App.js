import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import {BrowserRouter, Routes, Route} from "react-router-dom";
import {NavDropdown, Nav, Container, Button, Form, Navbar,} from 'react-bootstrap';
import HomePage from "./HomePage";
import categories from "./categories";

import React, {useState} from "react";
import SecondPage from "./SecondPage";
import ThirdPage from "./ThirdPage";

function App() {
  const [price, setPrice] = useState('')
  const [curen, setCuren] = useState('som')
  const [search, setSearch] = useState('')
  const [but, setBut] = useState('')


  const butclick = () => {
    setBut(search)
  }



  return (
    <>
      <BrowserRouter>
        <Navbar bg="light" expand="lg">
          <Container fluid>
            <Navbar.Brand href="/">Navbar scroll</Navbar.Brand>
            <Navbar.Toggle aria-controls="navbarScroll" />
            <Navbar.Collapse id="navbarScroll">
              <Nav
                className="me-auto my-2 my-lg-0"
                style={{ maxHeight: '100px' }}
                navbarScroll
              >
                <Nav.Link href="/">Home</Nav.Link>
                <Nav.Link href="/discount">Скидки</Nav.Link>
                <NavDropdown title="Категории" id="navbarScrollingDropdown">
                  {categories.map((prod =>  <NavDropdown.Item href={`/categories/${prod.id}`}>{prod.short_title}</NavDropdown.Item>))}

                </NavDropdown>
              </Nav>
              <Form className="d-flex">
                <Form.Control
                  type="search"
                  placeholder="Search"
                  className="me-2"
                  aria-label="Search"
                  onChange={(even) => {
                    setSearch(even.target.value)
                  }}
                />
                <Button variant="outline-success" onClick={butclick}>Search</Button>
              </Form>
            </Navbar.Collapse>
          </Container>
        </Navbar>






        <Routes>
          <Route path={"/"} element={<HomePage but={but} price={price} curen={curen} setCuren={setCuren}/>}/>
          <Route path={"/discount"} element={<SecondPage but={but} price={price} curen={curen} setCuren={setCuren}/>}/>
          <Route path={"/categories/:id"} element={<ThirdPage but={but} price={price} curen={curen} setCuren={setCuren}/>}/>
        </Routes>
      </BrowserRouter>

    </>
  );
}

export default App;
